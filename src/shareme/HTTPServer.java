package shareme;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import javafx.application.Platform;

public class HTTPServer implements Runnable {

    private final Socket connectionSocket;

    HTTPServer(Socket connectionSocket) {
        this.connectionSocket = connectionSocket;

    }

    public void handleAClient() throws IOException {

        DataInputStream din;
        try {
            din = new DataInputStream(connectionSocket.getInputStream());
        } catch (IOException ex) {
            throw new IOException("unexpected error at getting input stream: " + ex);
        }

        String message;
        try {
            message = din.readLine();
        } catch (IOException ex) {
            din.close();
            throw new IOException("unexpected error at reading line from input stream: " + ex);
        }

        if (message == null) {
            return;
        }

        DataOutputStream dout;
        try {
            dout = new DataOutputStream(new BufferedOutputStream(connectionSocket.getOutputStream()));
        } catch (IOException ex) {
            din.close();
            throw new IOException("unexpected error at getting output stream: " + ex);
        }

        String method = message.substring(0, message.indexOf(" "));

        if (method.equals("GET")) {
            String reqURL = message.substring(message.indexOf(" ") + 1, message.lastIndexOf(" "));
            reqURL = reqURL.replace("%20", " ");

            if (!reqURL.equals("/")) {
                reqURL = reqURL.substring(1);
            } else {
                reqURL = ".";
            }

            String path = reqURL;

            File file = new File(path);

            File parent = new File(".");
            String parentAdr = parent.getAbsolutePath().substring(0, parent.getAbsolutePath().length() - 1);

            if (file.getAbsolutePath().startsWith(parentAdr) && file.exists()) {

                if (file.isFile()) {
                    try {
                        //head
                        dout.writeBytes("HTTP/1.1 200 ok\r\n");
                        dout.writeBytes(new java.util.Date().toString() + "\r\n");
                        dout.writeBytes("Content-type: " + file.getName().substring(file.getName().lastIndexOf(".") + 1) + "\r\n");
                        dout.writeBytes("Content-length: " + file.length() + "\r\n");
                        dout.writeBytes("\r\n");

                        //body
                        dout.write(Files.readAllBytes(file.toPath()));
                    } catch (IOException ex) {
                        dout.close();
                        din.close();
                        throw new IOException("unexpected error at writing output stream for " + file.getName() + ": " + ex);
                    }
                } else if (file.isDirectory()) {
                    try {
                        //head
                        dout.writeBytes("HTTP/1.1 200 ok\r\n");
                        dout.writeBytes(new java.util.Date().toString() + "\r\n");
                        dout.writeBytes("\r\n");

                        //body
                        File[] subDirectory = file.listFiles();
                        for (int i = 0; i < subDirectory.length; i++) {
                            if (subDirectory[i].isDirectory()) {
                                dout.writeBytes("<a href=\"" + subDirectory[i].getPath() + "\">");
                                dout.writeBytes(subDirectory[i].getName());
                                dout.writeBytes("</a>");
                                dout.writeBytes("<br>");
                            }
                        }
                        for (int i = 0; i < subDirectory.length; i++) {
                            if (subDirectory[i].isFile()) {
                                dout.writeBytes("<a href=\"" + subDirectory[i].getPath() + "\">");
                                dout.writeBytes(subDirectory[i].getName());
                                dout.writeBytes("</a>");
                                dout.writeBytes("<br>");
                            }
                        }

                    } catch (IOException ex) {
                        dout.close();
                        din.close();
                        throw new IOException("unexpected error at writing output stream for " + file.getName() + ": " + ex);
                    }
                }
            } else {
                try {
                    dout.writeBytes("HTTP/1.1 404 NOT FOUND");
                    dout.writeBytes(new java.util.Date().toString() + "\r\n");

                } catch (IOException ex) {
                    dout.close();
                    din.close();
                    throw new IOException("unexpected error at writing output stream for 404: " + ex);
                }
            }
        } else {
            try {
                dout.writeBytes("HTTP/1.1 503 " + method + "not emplemeted\r\n");
                dout.writeBytes(new java.util.Date().toString() + "\r\n");
            } catch (IOException ex) {
                dout.close();
                din.close();
                throw new IOException("unexpected error at writing output stream for 503: " + ex);
            }
        }

        dout.close();
        din.close();

        try {
            connectionSocket.close();
        } catch (IOException ex) {
            throw new IOException("unexpected error at closing connectionSocket: " + ex);
        }

    }

    @Override
    public void run() {
        try {
            handleAClient();
        } catch (IOException ex) {
            Platform.runLater(() -> {
                ShareThisFolder.log.appendText(ex.toString() + "\n");
            });
        }
    }

}
