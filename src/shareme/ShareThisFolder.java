package shareme;

import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ShareThisFolder extends Application {

    private static HTTPServer server;
    private static ServerSocket setupSocket; // for tie client to server
    private static Socket connectionSocket;
    private static int port;
    private static Thread startThread;
    public static TextArea log;

    @Override
    public void start(Stage primaryStage) throws UnknownHostException {

        String ip = getLocalIP();

        Label ipLabel = new Label(ip + ":");
        TextField portTextFiled = new TextField("60000");
        portTextFiled.setPrefWidth(60);
        HBox intAddress = new HBox(7);
        intAddress.setAlignment(Pos.CENTER_RIGHT);
        intAddress.getChildren().addAll(ipLabel, portTextFiled);

        Label status = new Label("stopped");
        status.setFont(Font.font(50));

        Button startBtn = new Button("start");
        Button stopBtn = new Button("stop");
        startBtn.setDisable(false);
        stopBtn.setDisable(true);
        HBox btns = new HBox(50);
        btns.setAlignment(Pos.CENTER);
        btns.getChildren().addAll(startBtn, stopBtn);

        log = new TextArea();
        log.setEditable(false);

        startBtn.setOnAction((event) -> {
            startBtn.setDisable(true);
            stopBtn.setDisable(false);
            startThread = new Thread(() -> {

                try {
                    port = Integer.parseInt(portTextFiled.getText());
                    setupSocket = new ServerSocket(port);
                    Platform.runLater(() -> {
                        log.appendText("setting up server on port " + port + "..." + "\n");
                    });
                    Platform.runLater(() -> {
                        log.appendText("server is on... " + "\n");
                    });
                } catch (IOException | NumberFormatException ex) {
                    Platform.runLater(() -> {
                        log.appendText("failed to start server " + ex.toString() + "\n");
                    });
                    startBtn.setDisable(false);
                    stopBtn.setDisable(true);
                    Platform.runLater(() -> {
                        status.setText("Stopped");
                    });
                    Platform.runLater(() -> {
                        log.appendText("server is off... " + "\n");
                    });
                    return;
                }

                Platform.runLater(() -> {
                    status.setText("Running");
                });

                while (true) {

                    Platform.runLater(() -> {
                        log.appendText("-------------------------\nwaiting for client..." + "\n");
                    });

                    try {
                        connectionSocket = setupSocket.accept();
                    } catch (IOException ex) {
                        if (setupSocket.isClosed()) {
                            break;
                        }
                    }

                    Platform.runLater(() -> {
                        log.appendText("client: " + connectionSocket + " accepeted..." + "\n");
                    });

                    server = new HTTPServer(connectionSocket);

                    new Thread(server).start();
                }

            });
            
            startThread.start();
            
        });

        stopBtn.setOnAction((ActionEvent event) -> {
            startBtn.setDisable(false);
            stopBtn.setDisable(true);
            Platform.runLater(() -> {
                status.setText("Stopped");
            });
            Platform.runLater(() -> {
                log.appendText("server is off..." + "\n");
            });
            try {
                setupSocket.close();
            } catch (IOException ex) {
                Platform.runLater(() -> {
                    log.appendText("faild to shutdown server..." + "\n");
                });
            }

        });

        VBox root = new VBox(20);
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(status, btns, log, intAddress);
        root.setPadding(new Insets(15));

        Scene scene = new Scene(root, 700, 500);

        primaryStage.setResizable(false);
        primaryStage.setTitle("ShareThisFolder");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

//==============================================================================
    public static void main(String[] args) {

        launch(args);
        System.exit(0);
    }

//==============================================================================
    public String getLocalIP() {

        String ip = "";

        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp()) {
                    continue;
                }

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    if (addr instanceof Inet6Address) {
                        continue;
                    }

                    if (iface.getDisplayName().equals("Broadcom 802.11n Network Adapter")) {
                        ip = addr.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            return "";
        }

        return ip;
    }

}
